// ==UserScript==
// @name         RemedyModLoader
// @version      0.3.3
// @description  Various QoL/Fix mods for Remedy
// @run-at       document-start
// @namespace    https://gitlab.com/ItsJustSomeDude/remedy-mods/
// @updateURL    https://gitlab.com/ItsJustSomeDude/remedy-mods/-/raw/main/RemedyModLoader.user.js
// @downloadURL  https://gitlab.com/ItsJustSomeDude/remedy-mods/-/raw/main/RemedyModLoader.user.js
// @author       Matthias "ItsJustSomeDude" Feldpausch
// @match        https://usdacts.fed.onbmc.com/*
// @match        https://usdacts-qa.fed.onbmc.com/*
// @include      https://usdacts.fed.onbmc.com/*
// @include      https://usdacts-qa.fed.onbmc.com/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.min.js
// @require      https://dystroy.org/JSON.prune/JSON.prune.js
// @grant        none
// ==/UserScript==

/* global LZString */

let disabledMods = [ "alwaysUnlockWorkNote" ];
let enabledMods = [ "fixReportedSource", "timedTemplateInterupt", "skipSimplePeopleRecord" ];
let debugging = true;

let modsList = [
    {
        "name": "Tab Title View KBA",
        "id": "tabTitleViewKBA",
        "context": "kbaView",
        "description": "When on a KBA page, make the tab title into the KBA number",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 2000,
            "function": () => {
                try {
                    for(let i = 0; i<3; i++)
                    {
                        if( document.querySelector(`#arid_WIN_${i}_302300507`) )
                        {
                            let nextTitle = `View KBA - ${document.querySelector(`#arid_WIN_${i}_302300507`).value}`
                            if(document.title != nextTitle)
                            {
                                document.title = nextTitle;
                            }
                        }
                    }
                } catch(e) {}
            }
        }
    },
    {
        "name": "Tab Title Edit KBA",
        "id": "tabTitleEditKBA",
        "context": "kbaEdit",
        "description": "When on a KBA Edit page, make the tab title into the KBA number",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 2000,
            "function": () => {
                try {
                    for(let i = 0; i<3; i++)
                    {
                        if(window.fc.formContexts[0].fields[302300507].mDataVal.mValue)
                        {
                            let nextTitle = `View KBA - ${window.fc.formContexts[0].fields[302300507].mDataVal.mValue}`
                            if(document.title != nextTitle)
                            {
                                document.title = nextTitle;
                            }
                        }
                    }
                } catch(e) {}
            }
        }
    },
    {
        "name": "Unlock Reported Source",
        "id": "fixReportedSource",
        "context": "newTicket",
        "description": "Unlock the reported source field.  This is only needed if you have Remedy set to automatically create a new ticket.",
        "defaultEnabled": false,
        "loop": {
            "defaultTiming": 10000,
            "function": () => {
                try {
                    for(let i = 1; i < 4; i++)
                    {
                        if (document.querySelector(`#arid_WIN_${i}_1000000215`) && document.querySelector(`#arid_WIN_${i}_1000000215`).readOnly) {
                            document.querySelector(`#arid_WIN_${i}_1000000215`).readOnly = false;
                        }
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Remove Ignoreable Error Dialogs",
        "id": "removeMessages",
        "context": "warningDialog",
        "description": "Remove certain warning boxes that can be safely ignored.",
        "defaultEnabled": true,
        "awaitElement": ".btn.btn3d.PopupBtn",
        "loop": {
            "defaultTiming": 150,
            "function": () => {
                let matches = [
                    "ARWARN 10005",
                    "ARWARN 100023",
                    "ARWARN 1000024",
                    "ARWARN 10999"
                ]
                for(let match of matches) {
                    try {
                        if (document.querySelector("#PopupMsgBox").innerText.indexOf(match) != -1 && document.querySelector(".btn.btn3d.PopupBtn")) {
                            document.querySelector(".btn.btn3d.PopupBtn").click()
                        }
                    } catch (e) {}
                }
            }
        }
    },
    {
        "name": "Unlock Work Note",
        "id": "alwaysUnlockWorkNote",
        "context": "newTicket",
        "description": "Keep the work notes Public/Not locked by default.",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 2000,
            "function": () => {
                try {
                    if (!document.querySelector("#WIN_2_rc1id304247260").checked) {
                        document.querySelector("#WIN_2_rc1id304247260").click()
                        document.querySelector("#WIN_2_rc1id1000000761").click()
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Never Advanced KBA Search",
        "id": "neverAdvancedKbaSearch",
        "context": "newTicket",
        "description": "Make it so it will never try to use the Advanced KBA search.",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 500,
            "function": () => {
                try {
                    for (let i = 0; i < 25; i++) {
                        if (document.querySelector(`#WIN_${i}_rc0id302300643`) && document.querySelector(`#WIN_${i}_rc0id302300643`).checked) {
                            document.querySelector(`#WIN_${i}_rc0id302300643`).click()
                            document.querySelector(`#WIN_${i}_302259032`).click()
                        }
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "New Ticket Title",
        "id": "newTicketTitle",
        "context": "newTicket",
        "description": "Rename the New Ticket window to the summary of the ticket.",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 1000,
            "function": () => {
                try {
                    for(let i = 0; i<5; i++) {
                        if(document.querySelector(`#arid_WIN_${i}_1000000000`) && document.querySelector(`#arid_WIN_${i}_1000000161`))
                        {
                            let nextTitle = `New - ${document.querySelector(`#arid_WIN_${i}_1000000000`).value || "No Summary"} - ${document.querySelector(`#arid_WIN_${i}_1000000161`).value}`
                            if (document.title != nextTitle) {
                                document.title = nextTitle
                            }
                        }
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Existing Ticket Title",
        "id": "existingTicketTitle",
        "context": "existingTicket",
        "description": "Rename the Existing Ticket window to the summary of the ticket.",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 1000,
            "function": () => {
                try {
                    for(let i = 0; i<5; i++) {
                        if(document.querySelector(`#arid_WIN_${i}_1000000000`) && document.querySelector(`#arid_WIN_${i}_1000000161`))
                        {
                            let nextTitle = `View - ${document.querySelector(`#arid_WIN_${i}_1000000000`).value || "No Summary"} - ${document.querySelector(`#arid_WIN_${i}_1000000161`).value}`
                            if (document.title != nextTitle) {
                                document.title = nextTitle
                            }
                        }
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Search Ticket Title",
        "id": "searchTicketTitle",
        "context": "searchTicket",
        "description": "Rename the Search ticket window to the currently selected ticket..",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 1000,
            "function": () => {
                try {
                    for(let i = 0; i<5; i++) {
                        if(document.querySelector(`#arid_WIN_${i}_1000000000`) && document.querySelector(`#arid_WIN_${i}_1000000000`).value && document.querySelector(`#arid_WIN_${i}_1000000161`) && document.querySelector(`#arid_WIN_${i}_1000000161`))
                        {
                            let nextTitle = `Search - ${document.querySelector(`#arid_WIN_${i}_1000000000`).value || "No Summary"} - ${document.querySelector(`#arid_WIN_${i}_1000000161`).value}`
                            if (document.title != nextTitle) {
                                document.title = nextTitle
                            }
                            break
                        } else {
                            let nextTitle = `Search Incident`
                            if (document.title != nextTitle) {
                                document.title = nextTitle
                            }
                        }
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Incident Console Title",
        "id": "incidentConsoleTitle",
        "context": "incidentConsole",
        "description": "Rename the Incident Console tab.",
        "defaultEnabled": true,
        "init": {
            "delay": 1000,
            "function": () => {
                try {
                    let nextTitle = `BMC Remedy - Incident Console`
                    if (document.title != nextTitle) {
                        document.title = nextTitle
                    }
                } catch (e) {}
            }
        }
    },
    {
        "name": "Skip Simple People Record",
        "author": "Matthias F",
        "id": "skipSimplePeopleRecord",
        "context": "simplePeopleDialog",
        "description": "Automatically open the full people record box when opeing the small one.",
        "defaultEnabled": false,
        "awaitElement": "#WIN_0_536870913",
        "init": {
            "delay": 10,
            "function": () => {
                try {
                    if(document.querySelector("#WIN_0_536870913"))
                    {
                        document.querySelector("#WIN_0_536870913").click();
                    }
                } catch(e) {}
            }
        }
    },
    {
        "name": "Auto Auto-Assign",
        "author": "Matthias F",
        "id": "autoAutoAssign",
        "context": "autoAssignDialog",
        "description": "Automatically pick the first thing from the Auto-assign dialog.",
        "defaultEnabled": true,
        "awaitElement": "#WIN_0_301912800",
        "init": {
            "delay": 10,
            "function": () => {
                try {
                    if(document.querySelector("#WIN_0_301912800"))
                    {
                        document.querySelector("#WIN_0_301912800").click();
                    }
                } catch(e) {}
            }
        }
    },
    {
        "name": "Remove Template Box - Interupt Mode",
        "author": "Thomas S",
        "id": "timedTemplateInterupt",
        "context": "templateSelect",
        "description": "Close the Template Selection box after a second, as long as it is not interacted with by Clicking.",
        "awaitElement": ".Ref.btn",
        "defaultEnabled": false,
        "preInit": () => {
            document.querySelector("body").addEventListener("click", () => {
                clearTimeout(runningLoops.timedTemplateInterupt)
                clearTimeout(runningDelayInits.timedTemplateInterupt)
                clearTimeout(runningElementPollings.timedTemplateInterupt)
                // debug("You clicked.")
            }, true)
        },
        "init": {
            "delay": 2000,
            "function": () => {
                try {
                    if (document.querySelector(".btn.btn3d.arfid301614900.ardbnz3Btn_CloseForm")) {
                        document.querySelector(".btn.btn3d.arfid301614900.ardbnz3Btn_CloseForm").click()
                    }
                } catch(e) {}
            }
        }
    },
    {
        "name": "Mustache Fields Alpha 1",
        "author": "Matthias F",
        "id": "fieldMustache",
        "context": "newTicket",
        "description": "Allows for more powerful templating by replacing strings in {{}} to be replaced with dynamic data.",
        "defaultEnabled": true,
        "loop": {
            "defaultTiming": 200,
            "function": async () => {
                try {
                    let largeEditors = document.querySelectorAll("#editor, #arid_WIN_2_1000000151, #arid_WIN_2_304247080")

                    let toReplace = {}
                    toReplace.agency = document.getElementById("arid_WIN_2_1000000082").value;

                    if(window && window.fc && window.fc.Templates && window.fc.Templates["HPDCustomerTooltip.html"] && window.fc.Templates["HPDCustomerTooltip.html"].mParams )
                    {
                        toReplace.city = window.fc.Templates["HPDCustomerTooltip.html"].mParams.CITY;
                        toReplace.zip = window.fc.Templates["HPDCustomerTooltip.html"].mParams.CODE;
                        toReplace.name = window.fc.Templates["HPDCustomerTooltip.html"].mParams.CONTACT;
                        toReplace.email = window.fc.Templates["HPDCustomerTooltip.html"].mParams.EMAIL;
                        toReplace.phone = window.fc.Templates["HPDCustomerTooltip.html"].mParams.PHONE;
                        toReplace.state = window.fc.Templates["HPDCustomerTooltip.html"].mParams.STATE;
                        toReplace.address = window.fc.Templates["HPDCustomerTooltip.html"].mParams.STREET;
                        toReplace.site = window.fc.Templates["HPDCustomerTooltip.html"].mParams.SITE;
                    }

                    for(let editor of largeEditors) {
                        for(let key in toReplace)
                        {
                            //console.log(key)
                            if(toReplace[key])
                            {
                                editor.value = editor.value.replace(`{{${key}}}`, toReplace[key])
                            }
                        }
                    }
                } catch(e) { }
            }
        }
    }
]

let toggleMod = (modId) => {
    debug("I would have just toggled the mod here...")
}

let createModMenu = () => {

    let outputTable = document.createElement("table");
    let tableBody = document.createElement("tbody");
    let headerRow = document.createElement("tr");
    let headerText = document.createElement("th");

    headerText.innerHTML = "This is the header"
    headerText.colSpan = 7;
    headerRow.append(headerText)

    tableBody.append(headerRow)

    for(let mod of modsList)
    {
        let newRow = document.createElement("tr");

        let nameCell = document.createElement("td");
        let toggleCell = document.createElement("td");
        let toggleButton = document.createElement("button");

        nameCell.innerHTML = mod.name;
        nameCell.title = `${mod.description}\n\nCreated by ${mod.author || "Matthias F"}`

        if(disabledMods.includes(mod.id) || (!mod.defaultEnabled && !enabledMods.includes(mod.id))) {
            toggleButton.innerHTML = "Enable"
        } else {
            toggleButton.innerHTML = "Disable"
        }
        toggleCell.title = `Mod ID: "${mod.id}"${mod.loop && "\n\nMod Contains a Loop Function." || ""}${mod.init && "\n\nMod Contains an Init Function." || ""}`;

        toggleButton.onclick = (function(entry) {return function() {toggleMod(entry);}})(mod.id);

        toggleCell.append(toggleButton)

        nameCell.style.paddingLeft = "8px"
        nameCell.style.paddingRight = "8px"
        toggleCell.style.paddingLeft = "8px"
        toggleCell.style.paddingRight = "8px"

        newRow.append(nameCell);
        newRow.append(toggleCell);

        tableBody.append(newRow);
    }

    outputTable.append(tableBody)
    outputTable.border = 1;
    outputTable.id = "rmlModListTable"

    console.log(outputTable.outerHTML)
}

let getContext = () => {
    try {
        if (window.fc.formContexts[0].fields[302300507]) {
            return "kbaEdit"
        }

        if(document.querySelector("body").getAttribute("style") == "margin:0;background-color:transparent" ) {
            return "customerInfoPopup"
        }

        if(document.querySelector("#arid_WIN_0_302300507") || document.querySelector("#arid_WIN_1_302300507")) {
            return "kbaView"
        }

        let page = location.toString();
        if(page.includes("TemplateSPGLookUp")){
            return "templateSelect"
        } else if(page.includes("ping.html")) {
            return "pingFrame"
        } else if(page.includes("MessagePopup.html")) {
            return "warningDialog"
        } else if(page.includes("People/Dialog+View-Simplified")) {
            return "simplePeopleDialog"
        } else if(page.includes("People/Dialog+View")) {
            return "fullPeopleDialog"
        } else if(page.includes("Assignment/Selection")) {
            return "autoAssignDialog"
        }

        let plusEnabled = !document.querySelector("#reg_img_304276690").title.includes("Disabled");
        let searchEnabled = !document.querySelector("#reg_img_304276710").title.includes("Disabled");

        if( !plusEnabled && searchEnabled ) {
            return "newTicket"
        } else if( !plusEnabled && !searchEnabled ) {
            return "incidentConsole"
        } else if( plusEnabled && searchEnabled ) {
            return "existingTicket"
        } else if( plusEnabled && !searchEnabled ) {
            return "searchTicket"
        }
    } catch(e) {
        return "broken"
    }
    return "unknown"
}

let loadMod = (mod) => {
    if(mod.init) {
        //modDebug(mod, "Loading Init Component.")

        if(typeof mod.init.function != "function")
        {
            modError(mod, "init Function is not a function.")
            return
        }

        if(mod.init.delay)
        {
            modDebug(mod, `init component will run after ${mod.init.delay} ms`);
            runningDelayInits[mod.id] = setTimeout(mod.init.function, mod.init.delay)
        } else {
            mod.init.function()
            modDebug(mod, "init component executed")
        }
    }

    if(mod.loop) {
        let loopTiming = 10000;
        //modDebug(mod, "Loading Loop Component.")

        if(typeof mod.loop.function != "function")
        {
            modError(mod, "loop function is not a function.")
            return
        }

        if(!mod.loop.defaultTiming)
        {
            modDebug(mod, `Loop has no defaultTiming value.  Defaulting to 10 seconds.`)
        } else {
            loopTiming = mod.loop.defaultTiming;
        }

        runningLoops[mod.id] = setInterval(mod.loop.function, loopTiming)
        modDebug(mod, "Loop Component Activated.")
    }

    if(!mod.loop && !mod.init) {
        modError(mod, "Mod has no loop and no init function.")
    }
}

let queueModLoad = (mod) => {
    if(mod.preInit) {
        mod.preInit();
        modDebug(mod, "Ran mod Pre-init function.")
    }

    if(mod.awaitElement) {
        runningElementPollings[mod.id] = setInterval(() => {
            try {
                if(document.querySelector(mod.awaitElement)) {
                    clearInterval(runningElementPollings[mod.id])
                    loadMod(mod);
                }
            } catch(e) {}
        }, 25)
        modDebug(mod, `Mod will run after an element matching "${mod.awaitElement}" exists.`)
    } else {
        loadMod(mod);
    }
}

let modDebug = (mod, message, full = false) => {
    if(debugging)
    {
        if(full){
            console.log(`[RML ${thisId} Debug]:`, mod, `: ${message}`)
        } else {
            console.log(`[RML ${thisId} Debug]: ${mod.id}: ${message}`)
        }
    }
}

let debug = (message) => {
    if(debugging)
    {
        console.log(`[RML ${thisId} Debug]:`, message)
    }
}

let modError = (mod, message) => {
    console.error(`[RML ${thisId} Error]:`, mod, ":", message)
}

let runningLoops = {};
let runningDelayInits = {};
let runningElementPollings = {};

let loadCurrentContextMods = () => {
    let currentContext = getContext()

    if(currentContext == "pingFrame")
    {
        clearInterval(contextPolling)
        return
    } else {
        debug("Loading mods for context: " + currentContext)
    }

    for(let mod of modsList)
    {
        if(disabledMods.includes(mod.id) || (!mod.defaultEnabled && !enabledMods.includes(mod.id))) {
            modDebug(mod, "Disabled.  Skipping.")
            continue;
        } else if(mod.context != currentContext) {
            // modDebug(mod, "Not For Current Context.  Skipping.")
            continue;
        }

        queueModLoad(mod);
    }

    if(GM_info && GM_info.script)
    {
        debug("Completed loading RemedyModLoader version " + GM_info.script.version)
    } else {
        debug("Completed loading RemedyModLoader version unknown.",)
    }
}

let lastContext;

let contextPollingLoop = () => {
    let currentContext = getContext()

    if(currentContext != lastContext)
    {
        debug(`Last context was ${lastContext} new is ${currentContext}`)
        loadCurrentContextMods()
    } else {
        // debug("Current Context: " + currentContext)
    }
    lastContext = currentContext;
}

let addStyle = (css, id) => {
    let node = document.createElement("style");
    node.type = "text/css";
    if(id) node.id = id;
    node.appendChild(document.createTextNode(css));
    let heads = document.getElementsByTagName("head");
    if (heads.length > 0) {
        heads[0].appendChild(node);
    } else {
        // no head yet, stick it whereever
        document.documentElement.appendChild(node);
    }
}


let contextPolling;
let thisId;

(function() {
    // WE DON'T WANT TO RUN ON THE PING FRAME, SHEESH.
    if(location.href.includes("ping.html")) return

    thisId = Math.floor(Math.random() * 10000);

    debug("Actual Start", location.href)
    let initRML = (event) => {
        setTimeout(() => {
            debug("Start of RML on" + location.href)

            lastContext = getContext()

            contextPolling = setInterval(contextPollingLoop, 2000)

            //console.log("[RML Debug]: window:", window)
            loadCurrentContextMods()
        }, 1)
    };

    window.addEventListener('DOMContentLoaded', initRML, { once: true });

})();
