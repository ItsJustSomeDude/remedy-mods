# RemedyMods

Welcome to the repository for RemedyMods.  This is a UserScript tool for injecting custom code ("mods") into BMC Remedy.  It works best on sites that work as SPAs, but also have window.open() popups.

## Mod Format
There is an Array, `modsList`, that holds all the mod data and code.  The format of the object is below.
```js
{
    "name": "Hello World",          // User-friendly name of your mod.
    "author": "ItsJustSomeDude",    // You! 
    "id": "helloWorld",             // The mod Id.  This is what you use to stop the mod from your code, enable and disable it, etc.
    "context": "newTicket",         // What context your mod will run in.  See the Contexts section for info.
    "description": "Log a Hello World message when loading the page, 5 seconds after the page loads, and then every 10 seconds.",       // Simply describe what your mod does here.
    "defaultEnabled": true,         // Should the mod be enabled by default
    "awaitElement": "body",         // (Optional) CSS selector that has to exist on the page before loading the init or loop functions.
    "preInit": () => {              // (Optional) This will be run as soon as possible, even before the awaitElement exisits.
        console.log("Hello World from preInit!")
    },
    "loop": {                       // (Optional) This will run over and over until stopped.
        "defaultTiming": 10000,     // (Optional) How ofter the loop will run.  If ommited, will default to 10 seconds.
        "function": () => {         // Function to loop run.
            console.log("Hello World from the 10 second loop!")
        }
    },
    "init": {                       // (Optional) This will run when the page loads.
        "delay": 5000,              // (Optional) How much to delay the init function.
        "function": () => {         // Function to run.
            console.log("Hello World from the delayed init function!")
        }
    }
    // You must include either an Init function or a loop function.
}
```

## API
There are a few methods that you have access to from inside mod code.

To stop a mod during runtime, the following commands can be used.
```js
// To stop a delayed init operation
clearTimeout(runningDelayInits.modId)

// To stop a running loop operation
clearInterval(runningDelayInits.modId)

// To stop a mod that is waiting for it's awaitElement
clearInterval(runningElementPollings.modId)

// You can stop your own mods this way, or you can stop functions of other mods.
```
