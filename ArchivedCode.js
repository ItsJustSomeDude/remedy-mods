modsList.push({
    "name": "Autocopy Customer Information",
    "author": "Matthias F",
    "id": "grabCustomerInfo",
    "context": "customerInfoPopup",
    "description": "Automaticly copy the customer information when you open the tooltip for it.",
    "defaultEnabled": true,
    "loop": {
        "defaultTiming": 400,
        "function": async () => {
            try {
                // Yo, need a tab?  Get one here!: "	"
                let name = document.querySelector('table[valign="top"] > tbody > tr:nth-child(1) > td:nth-child(2)').innerText
                let site = document.querySelector('table[valign="top"] > tbody > tr:nth-child(1) > td:nth-child(4)').innerText
                let address = document.querySelector('table[valign="top"] > tbody > tr:nth-child(2) > td:nth-child(4)').innerText
                let phone = document.querySelector('table[valign="top"] > tbody > tr:nth-child(3) > td:nth-child(2)').innerText
                let email = document.querySelector('table[valign="top"] > tbody > tr:nth-child(4) > td:nth-child(2)').innerText

                let output = [
                    `Name:	${name}`,
                    `Phone:	${phone}`,
                    `Email:	${email}`,
                    `Agency:	{{agency}}`,
                    `Location:	${address}`,
                    "Building:	",
                    "Room/Cube:	",
                    "Computer Name:	",
                    "IP Address:	",
                    "Work Stoppage:	",
                    "New or Existing Ticket:	{{existing}}",
                    "Exact Problem Details:	"
                ].join("\n")
                //console.log(output)
                await navigator.clipboard.writeText(output);
                document.getElementById("grabCustomerInfo-Message").innerHTML = "Copied!"
                clearTimeout(runningLoops.grabCustomerInfo)
            } catch(e) { }
        }
    },
    "init": {
        "delay": 300,
        "function": () => {
            try {
                document.querySelector("body > div:nth-child(1)").insertAdjacentHTML("beforeEnd", "<p id=\"grabCustomerInfo-Message\" style=\"margin-left:8px; color: black;\">Click anywhere to copy.</p>")
            } catch(e) {}
        }
    }
})
